import org.junit.jupiter.api.Test;

import java.io.File;
import java.util.HashMap;

import static org.junit.jupiter.api.Assertions.*;

public class allTests {

    @Test
    void firstTest(){
        File file = new File("C:/Desktop/testJava/text1.txt");
        assertEquals(15, Reading.countingLines(file));
        assertEquals(20, Reading.countingWords(file));
        HashMap<String, Integer> map = new HashMap<>();
        map.put("super", 3);
        map.put("de", 1);
        map.put("fichier", 2);
        map.put("voyons", 1);
        map.put("si", 1);
        map.put("premier", 1);
        map.put("le", 1);
        map.put("suis", 1);
        map.put("texte", 2);
        map.put("il", 2);
        map.put("je", 3);
        map.put("marche", 2);
        assertEquals(map, Reading.occurence(file)); // fonctionne dans IntelliJ mais ne peut pas être exporter à cause du ç
 }
    @Test
    void secondTest(){
        File file = new File("C:/Desktop/testJava/text3.txt");
        assertEquals(9,Reading.countingLines(file));
        assertEquals(15, Reading.countingWords(file));
    }

    @Test
    void thirdTest(){
        File file = new File("C:/Desktop/testJava/longFichier.txt");
        assertEquals(10000, Reading.countingWords(file));
    }
}
