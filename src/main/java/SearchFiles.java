import java.io.File;

public class SearchFiles {

    public static File[] findFiles(String path){

            File directory = new File(path);

            if(directory.isDirectory()) {
                File[] files = directory.listFiles();
                return files;
            }else{

                File[] files = new File[1];
                files[0] = directory;
                return files;
            }
    }
}
