import java.io.File;
import java.util.HashMap;

public class CallMethode {


    public static void allCouting(String path){

        double timeStart = System.currentTimeMillis();
        double timeEnd = 0;
        try {

            File[] files = SearchFiles.findFiles(path);

            int numberWords = 0;
            int numberLines = 0;

            System.out.println(path + " has " + files.length + " files or directories");


            for (File file : files) {

                if (file.isDirectory()) {
                    System.out.println(file.getPath() + " is a directory !");
                    allCouting(file.toString());
                    continue;
                }

                if(file.getName().endsWith(".txt")) {


                    numberLines = Reading.countingLines(file);
                    numberWords = Reading.countingWords(file);
                    HashMap<String, Integer> map = Reading.occurence(file);

                    Print.printInfo(numberLines, numberWords, map, file);
                    timeEnd = System.currentTimeMillis();
                    System.out.println("\nTime take for the operation was " + (timeEnd - timeStart)/1000 + " seconds");
                }else{
                    System.out.println("\n" + file + " NON VALID FILE EXTENSION");
                }
            }
        }catch (NullPointerException e){
            System.out.println("PATH IS INCORECT");
        }

    }
}
