import java.io.File;
import java.util.HashMap;
import java.util.Map;

public class Print {

    public static void printInfo(int numberOfLines, int numberOfWords, HashMap<String, Integer> map, File file){

        int alignement = 0;

        System.out.println("\n" + file + " contains " + numberOfLines + " lines and " + numberOfWords + " words" + "\nand words occurence is :");

        for(Map.Entry entry : map.entrySet()){

            if(alignement % 9 == 0 && alignement != 0){
                System.out.println();

            }
                System.out.format("%-22s","| " + entry.getKey()+ " => "+ entry.getValue());
             alignement++;
        }
        System.out.println();
    }
}
