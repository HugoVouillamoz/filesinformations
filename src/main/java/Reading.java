import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;

public class Reading {


    public static int countingLines(File file){

        int numberLines = 0;

        try {
            BufferedReader reader = new BufferedReader(new FileReader(file));

            while(reader.readLine() != null){
                numberLines++;
            }

        }catch(IOException e){
           System.out.println("UNABLE TO COUNT THE NUMBER OF LINE");
        }

        return numberLines;
    }

    public static int countingWords(File file){

        int numberWords = 0;
        String str = "";

        try {
            Scanner scan = new Scanner(new FileInputStream(file));

            scan.toString().replaceAll("[ !\"\\#$%&()*+,-./:;<=>?@\\[\\]^_{|}]+" , " ");

            while(scan.hasNext()){
                scan.next();
                numberWords++;
            }

        }catch (IOException e){
            System.out.println("UNABLE TO COUNT THE NUMBER OF WORDS");
        }

        return numberWords;
    }

    public static HashMap occurence(File file){

        HashMap<String, Integer> map = new HashMap<>();

        String line;
        String [] wordList;

        try {
            BufferedReader data = new BufferedReader(new FileReader(file));
            StringBuilder text = new StringBuilder();

            while((line = data.readLine())  != null) {

                if(line != null) {
                    text.replace(0, text.length(), " " + line);


                    wordList = text.toString().split("[ !\"\\#$%&()*+,-./:;<=>?@\\[\\]^_{|}]+");

                    for (String str : wordList) {
                        if (str == "") {
                            continue;
                        }
                        str = str.toLowerCase();

                        Integer numberOccurence = map.get(str);

                        if (numberOccurence == null) {
                            map.put(str, 1);
                        } else {
                            map.put(str, numberOccurence + 1);
                        }
                    }
                }
            }

        }catch (IOException e){
            System.out.println("UNABLE TO COUNT OCCURENCE OF WORDS");
        }
        return map;

    }


}
